/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 4
 Christopher Kwok
 Jeffrey Delfin
 Lisbeth Lazala
 Sarena Tran
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	unsigned long sides[3] = {s1, s2, s3};
	sort(sides, sides+3);
	return ((sides[0] * sides[1])/2);
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
		unsigned long t1sides[3] = {t1.s1,t1.s2,t1.s3};
		sort(t1sides,t1sides+3);
		unsigned long t2sides[3] = {t2.s1,t2.s2,t2.s3};
		sort(t2sides,t2sides+3);
		if (t1sides[0] == t2sides[0]){
			if (t1sides[1] == t2sides[1]){
					if(t1sides[2] == t2sides[2]){
						return true;
					}
				}
			}
	return false;
}

bool similar(triangle t1, triangle t2) {
	// TODO: write this function.
	double t1sides[3]={t1.s1,t1.s2,t1.s3};
	sort (t1sides,t1sides+3);

	double t2sides[3]={t2.s1,t2.s2,t2.s3};
	sort (t2sides,t2sides+3);

	double ratio;
	double ratio2;
	double ratio3;

	ratio=(t1sides[0]/t2sides[0]);
	ratio2=(t1sides[1]/t2sides[1]);
	ratio3=(t1sides[2]/t2sides[2]);

	if ((ratio)==(ratio2)==(ratio3)) {
		return true;
	}
	return false;
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h
	 // storage for return value.
vector<triangle> retval;
	unsigned long i,j,k;
	for(i = 1; i <h;i++){
		for (j=1; j<h;j++){
			for (k=1; k<h; k++){
				if(i+j > k&& i+k > j && j+k>i){
					unsigned long p = i + j +k;
					if (p >= l && h >= p){
						int sides[3] = {i,j,k};
						sort(sides, sides+3);
						if ((sides[0] * sides[0]) +(sides[1] * sides[1]) ==(sides[2] * sides[2])){
							triangle z = triangle(sides[0],sides[1],sides[2]);
							int g=0;
							for (unsigned long n =0; n < retval.size(); n++){
								if (congruent(retval[n],z)){
									g++;
								}
							}
							if(g==0){
								retval.push_back(z);
							}
						}
					}
				}
			}

	}
}
		return retval;
}

